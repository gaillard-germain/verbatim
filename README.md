# verbatim

![Verbatim screenshot](verbatim.png "Verbatim screenshot")

## About
Verbatim is a game of letters running in your web browser. The goal is to find a word from a series of letters. The game is playable in French or English.

## Programing languages
Verbatim was coded with Python3, Flask framework, JavaScript (JQuery), HTML5, CSS3.

## Install
- Fork it from [GitLab](https://gitlab.com/gaillard.germain/verbatim.git), clone it on your computer.
- Create a virtual environnement.
`python3 -m venv <your venv name>`
- Activate your venv.
`source <your venv name>/bin/activate`
- Install dependencies.
`pip install -r requirements.txt`
- Run it.
`python run.py`
- Then open your web browser at [http://127.0.0.1:5000/](http://127.0.0.1:5000/)

## License
Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
