from random import choices
import json


class Verbatim:

    def __init__(self):
        self.lang = ''
        self.letters = None
        self.points = {}
        self.valid_words = None
        self.pouch_capacity = 9
        self.total_score = 0
        self.total_ai_score = 0
        self.pouch = []
        self.solution = {}
        self.word = ''
        self.score = 0
        self.ai_word = ''
        self.ai_score = 0
        self.warning = ''
        self.load_data('fr')
        self.deal_letters()

    def create_points_dict(self):
        """ creates the dict which set the points for each letter """

        # sort letters dict by values descending.
        letters = sorted(self.letters.items(), key=lambda x: x[1],
                         reverse=True)
        # list of sorted letters.
        letters = [letter[0] for letter in letters]
        # iterates through the list by 3 letters interval,
        # each interval increases the points by 2.
        step = 3
        point = 1
        for i in range(0, len(letters), step):
            for letter in letters[i:i+step]:
                self.points[letter] = point
            point += 2

    def load_data(self, lang):
        """ load data according to language """

        self.lang = lang
        file_path = "resources/letters_{}.json".format(lang)
        with open(file_path) as file:
            self.letters = json.load(file)
        file_path = "resources/words_{}.json".format(lang)
        with open(file_path) as file:
            self.valid_words = json.load(file)
        self.create_points_dict()

    def deal_letters(self):
        """ distributes letters randomly according to their frequency """

        if self.warning == 'no_solution' or self.warning == 'skipped':
            self.pouch.clear()
        elif self.word:
            for letter in self.word:
                self.pouch.remove(letter)
        self.pouch += choices(
            list(self.letters),
            weights=self.letters.values(),
            k=self.pouch_capacity - len(self.pouch)
        )
        self.solve()

    def scoring(self, word):
        """ calculate the score for each word """

        score = len(word)
        for letter in word:
            score += self.points[letter]
        return score

    def solve(self):
        """ find the solutions for each draw """

        self.solution.clear()
        for word in self.valid_words:
            if len(word) <= len(self.pouch):
                pouch = self.pouch.copy()
                for index, letter in enumerate(word):
                    if letter in pouch:
                        pouch.remove(letter)
                        if index == len(word) - 1:
                            self.solution[word] = self.scoring(word)
                    else:
                        break
        if not self.solution:
            self.warning = "no_solution"
            self.deal_letters()

    def check_word(self, word):
        """ checks if the word is in the solutions """

        if word in self.solution:
            self.word = word
            self.score = self.solution[self.word]
            self.ai_word = self.ai_gets_word()
            self.ai_score = self.solution[self.ai_word]
            self.warning = ''
            self.total_score += self.score
            self.total_ai_score += self.ai_score
            self.deal_letters()
        else:
            self.warning = "invalid_word"

    def skip(self):
        """ the user passes his turn """

        self.word = '...'
        self.ai_word = self.ai_gets_word()
        self.score = -self.solution[self.ai_word]
        self.ai_score = self.solution[self.ai_word]
        self.warning = "skipped"
        self.total_score += self.score
        self.total_ai_score += self.ai_score
        self.deal_letters()

    def ai_gets_word(self):
        """ returns one of the best words of the solution """

        return choices(
            list(self.solution),
            weights=self.solution.values(),
            k=1
        )[0]

    def reset_all(self):
        """ resets game data """

        self.word = ''
        self.score = 0
        self.ai_word = ''
        self.ai_score = 0
        self.warning = ''
        self.total_score = 0
        self.total_ai_score = 0
        self.solution.clear()
        self.pouch.clear()
        self.deal_letters()
