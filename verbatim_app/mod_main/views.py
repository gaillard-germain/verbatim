from flask import Blueprint, render_template, request, jsonify
from .verbatim import Verbatim


mod_main = Blueprint('main', __name__, url_prefix='/')

verbatim = Verbatim()

@mod_main.route('/', methods=['GET', 'POST'])
def board():
    if request.method == 'POST':
        lang = request.form['lang']
        restart = request.form['restart']
        word = request.form['word'].upper()

        if lang != verbatim.lang:
            verbatim.load_data(lang)
        if restart == 'yes':
            verbatim.reset_all()
        else:
            if word:
                verbatim.check_word(word)
            else:
                verbatim.skip()

        response = {
            "lang": verbatim.lang,
            "letters": verbatim.letters,
            "points": verbatim.points,
            "total_score": verbatim.total_score,
            "total_ai_score": verbatim.total_ai_score,
            "pouch": verbatim.pouch,
            "word": verbatim.word,
            "score": verbatim.score,
            "ai_word": verbatim.ai_word,
            "ai_score": verbatim.ai_score,
            "warning": verbatim.warning
        }
        response = jsonify(response)
        response.status_code = 200
        return response

    else:
        if verbatim.lang == 'fr':
            return render_template('main/board_fr.html', verbatim=verbatim)
        else:
            return render_template('main/board_en.html', verbatim=verbatim)
