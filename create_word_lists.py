from unidecode import unidecode
import re
import json


def create_word_list(lang):
    """ dumps the list of words into json file """

    valid_words = []
    if lang == 'fr':
        file = '/usr/share/dict/french'

    elif lang == 'en':
        file = '/usr/share/dict/british-english'

    else:
        print("No dict for this language")
        return 0

    with open(file) as word_file:
        words = set(word_file.read().split())

    for word in words:
        word = unidecode(word)
        if re.match(r"^[a-z]+$", word) and len(word) > 2:
            valid_words.append(word.upper())

    valid_words = list(set(valid_words))
    valid_words.sort()

    outfile_path = "resources/words_{}.json".format(lang)
    with open(outfile_path, "w") as outfile:
        json.dump(valid_words, outfile, indent=4)

    print("Successfully created {}".format(outfile_path))


if __name__ == '__main__':
    create_word_list('fr')
    create_word_list('en')
